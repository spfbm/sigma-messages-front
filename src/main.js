// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'bootstrap';
import Vue from 'vue';
import axios from 'axios';
import App from './App';
import router from './router';

axios.interceptors
  .request.use((config) => {
    // eslint-disable-next-line
    console.log(`Request: ${JSON.stringify(config)}`);
    return config;
  });
axios.interceptors
  .response.use((res) => {
    // eslint-disable-next-line
    console.log(`Response: ${JSON.stringify(res)}`);
    return res;
  });
axios.defaults.baseURL = 'http://localhost:3002';

Vue.config.productionTip = false;
// eslint-disable-next-line import/prefer-default-export
export const eventBuss = new Vue();
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
  // components: { App },
  // template: '<App/>',
});
