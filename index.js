const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');
const app = new Koa();
require('./scripts/services/database/mongo-connection.js')(app);
const router = new Router();
app.use(bodyParser())


app.use(async (ctx, next) => {
    try {
        await next();
    } catch(error) {
        ctx.status = 404;
        console.log(error.message);
        ctx.boxy = {status: 'ko', error};
    }
});

app.use(cors({
    origin: function(ctx) {
      if (ctx.url === '/test') {
        return false;
      }
      return '*';
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
    origin: '*'
}));


router
        .get('/', async(ctx, next) => {
            console.log(`Hellow world!`);
            ctx.status = 200;
            ctx.body = 'Hello world!';
        })

        .get('/users', async (ctx, next) => {
            console.log(`The query string is: ${ctx.query.personId}`);
            let result = [];
            if (ctx.query.personId != null) {
                let query = {personId: ctx.query.personId};
                result = await ctx.app.user.find(query).toArray();
            } else {
                result = await ctx.app.user.find().toArray();
            }
            ctx.status = 200;
            ctx.body = result;
        })
        .post('/users', async(ctx, next) => {
            console.log(`The rquest body: ${ctx.request.body}`);
            ctx.status = 201;
            ctx.body = await ctx.app.user.insertMany(ctx.request.body);
        })

app
        .use(router.routes())
        .use(router.allowedMethods());

app.listen(3002);
console.log(`Listening to the port 3002`);
