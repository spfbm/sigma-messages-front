const fetch = require('node-fetch');


var obsequesServerOptions = {
    method: 'POST',
    headers : {
        'Content-Type': 'application/json',
    },
};


exports.callService = async (requestBody, res) => {
    const baseUrl = 'http://oav-meccano-rest-d04.swarm-06.recette.macif.fr/api/distribution/prevoyance';
    var url = baseUrl + requestBody.endServicePath;
    obsequesServerOptions.body = JSON.stringify(requestBody.input);
    obsequesServerOptions.headers.Authorization = 'Bearer ' + `${requestBody.token}`
    const response = await fetch(url, obsequesServerOptions);
    const jsonResponse = await response.json();
    if (response.ok) {
        if(jsonResponse.propositions.length < 1) {
            return {status: 'KO', details: {reason: "Aucune proposition n'est retournée."}};
        } else {
            return {status: 'OK', details: {jsonResponse, kms_url: url}};
        }
    } else {
        throw JSON.stringify({ "status": "ko", details: {responseStatus:`${response.status}`, "message": jsonResponse.message}});
    }

}
