const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const app = new Koa();
const router = new Router();
require('./database/mongo-connection.js')(app);
app.use(bodyParser());

app.use(async (ctx, next) => {
    try {
        await next();
    } catch (error) {
        ctx.status = 404;
        console.log(error.message)
        ctx.body = {status: "ko", error};
    }
});


router
    .get("/persons", async (ctx, next) => {
        console.log("The url: " + ctx.url);
        let result = [];
        if (ctx.query.id != null) {
            let query = {id : ctx.query.id};
            result = await ctx.app.message.find(query).toArray();
        } else {
            result = await ctx.app.message.find().toArray();
        }
        ctx.status = 200;
        ctx.body = result;
    })

    .get("/", async (ctx, next) => {
        console.log("Hello world");
        ctx.status = 200;
        ctx.body = "Hello World";
    })

    .post("/persons", async (ctx, next) => {
        console.log("The body is :" + ctx.request.body);
        ctx.status = 201;
        ctx.body = await ctx.app.message.insertMany(ctx.request.body)
    })
;

app
    .use(router.routes())
    .use(router.allowedMethods());

const port = process.env.PORT || 8081
app.listen(port, () => {
    console.log(`listening on ${port}`);
});