const MongoClient = require('mongodb').MongoClient, url = "mongodb://localhost:27017/";
module.exports = function (users) {
    MongoClient.connect(url, {useUnifiedTopology: true})
        .then((client) => {
            const db = client.db('users');
            db.createCollection('errors', function (err, res) {
                if (err) {
                    throw err;
                }
            });
            users.user = db.collection('users');
            console.log("Database connection established")
        }).catch((err) => console.error(err))
};