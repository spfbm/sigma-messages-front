const Koa = require('koa');
const Router = require('koa-router');
const cors = require('koa2-cors');
const bodyParser = require('koa-bodyparser');
const app = new Koa();
const router = new Router();


const tarifObseques = require('./scripts/services/apiCall.js');

// Méthode principale permettant de gérer les erreurs et enchainer sur les autres traitements
app
    .use(async (ctx, next) => {
        try{
            await next();
        } catch (error) {
            ctx.status = 404;
            console.log(error.message)
            ctx.body = {status:"ko", error};
        }
   })
   .use(bodyParser())
   .use(cors({
        origin: function(ctx) {
            if (ctx.url === '/test') {
            return false;
            }
            return '*';
        },
        exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
        maxAge: 5,
        credentials: true,
        allowMethods: ['GET', 'POST', 'DELETE', 'OPTIONS'],
        allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
        contentType: 'application/json',
        origin: '*'
   }));


router
    .post('/', async (ctx, next) => {
        const data = await tarifObseques.callService(ctx.request.body, ctx.res, ctx.query.server);
        ctx.status = 200;
        ctx.body = data;
    })
;

// app.use( mount( '/', serve('./public') ) ) ;

app
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(3002);
console.log("Listening to http://localhost:3002");